# SET UP ECK
## 1. Install ECK Operator
### 1.1 Install custom resource definitions:
```
kubectl create -f https://download.elastic.co/downloads/eck/2.6.1/crds.yaml

```
**Result**
```
The following Elastic resources have been created:
customresourcedefinition.apiextensions.k8s.io/agents.agent.k8s.elastic.co created
customresourcedefinition.apiextensions.k8s.io/apmservers.apm.k8s.elastic.co created
customresourcedefinition.apiextensions.k8s.io/beats.beat.k8s.elastic.co created
customresourcedefinition.apiextensions.k8s.io/elasticmapsservers.maps.k8s.elastic.co created
customresourcedefinition.apiextensions.k8s.io/elasticsearches.elasticsearch.k8s.elastic.co created
customresourcedefinition.apiextensions.k8s.io/enterprisesearches.enterprisesearch.k8s.elastic.co created
customresourcedefinition.apiextensions.k8s.io/kibanas.kibana.k8s.elastic.co created
```
### 1.2 Install the operator with its RBAC rules:
```
kubectl apply -f https://download.elastic.co/downloads/eck/2.6.1/operator.yaml
```
>The ***ECK operator runs by default*** in the elastic-system ***namespace***. ***It is recommended that you choose a dedicated namespace for your workloads***, rather than using the elastic-system or the default namespace.

### 1.3 (Optional) Monitor the operator logs:
```
kubectl -n elastic-system logs -f statefulset.apps/elastic-operator
```



## 2. Install Kibana, Fleet & Elastic search
> **NOTE!** Storage size should be edited to suit your requirements and don't forget to change loadBalancer IP to your loadBalancer IP

<div style="text-align:center"><img src="/ELK Stack/image/size.png" /></div>

```
Kubectl apply -f elastic-kibana-fleet.yaml
```
Edit ***Services*** of 'Kibana', 'Elasticsearch-http', 'fleet-server-http' to ***LoadBlancer***

<div style="text-align:center"><img src="/ELK Stack/image/status_show.png" /></div>


### 2.1 Access to Kibana
Get secret to login to kibana
```
kubectl get secret elasticsearch-es-elastic-user -o=jsonpath='{.data.elastic}' | base64 --decode; echo
```
<div style="text-align:center"><img src="/ELK Stack/image/get_pass.png" /></div>

>***User*** : elastic
>***Pass*** : from secret above

## 3. Deploy apm-server
```
kubectl apply -f apm-server.yaml
```
## 4. Access to Kibana
Go to http://`IP-kibana-kb-http`:5601
<div style="text-align:center"><img src="/ELK Stack/image/kibana_start.png" /></div>

### 4.1 Create fleet
Search `inegrations` in search bar after that click on `Elastic APM`
<div style="text-align:center"><img src="/ELK Stack/image/integrations01.png" /></div>

Click `Elastic APM in Fleet` then go to `Manage APM integration in Fleet`
<div style="text-align:center"><img src="/ELK Stack/image/integrations02.png" /></div>

Add Elastic APM.
Click `Elastic APM in Fleet` then go to `Manage APM integration in Fleet`
<div style="text-align:center"><img src="/ELK Stack/image/integrations03.png" /></div>

Find General ,Change `Host` and `URL` to your `APM 's IP`.After that Save.
<div style="text-align:center"><img src="/ELK Stack/image/integrations04.png" /></div>

go to Integrations -> APM ,Click `Check APM Server status`. If it show green then your Elastic have connected to your APM.
<div style="text-align:center"><img src="/ELK Stack/image/integrations05.png" /></div>
