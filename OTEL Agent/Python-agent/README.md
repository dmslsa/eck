# Python-agent
>## REMINDER
>Before attach agent in your WebApp, You must deploy ECK and OTEL Collector first. By follows the [ECK manual](https://gitlabii.thaibevapp.com/devops/ea/eck/-/tree/master/ELK%20Stack) and [OTEL Collector manual](https://gitlabii.thaibevapp.com/devops/ea/eck/-/tree/master/OTEL%20Collector) 
>**Requirement:**
> - APM server
> - ECK Stack
> - Fleet server that connect to APM
> - OTEL Collector
> - Python >= 3.9
> - [pip](https://stackabuse.com/how-to-install-pip-on-windows/) >= 20.2.3
## Config
Go to `core` -> `.env`, Change `ENDPOINT` to your collecter' s IP


# Pet Marketplace
This is a sample flask full stack web application that allows user to buy pets. 

Application files are structured inside core package.

The application consists of three pages.

1. Welcome Page:
    1. In this page, user will enter their name and then they will be redirected to Home Page.
    
2. Home Page:
    1. Here, user can select the pet they want to buy. They are given two options; Dog and Cat.
    2. Whenever user buys the pet, on the right side of the page user can see list of their pets with their serial numbers and sound
    
3. Admin Page:
    1. In the welcome page, when the user enters the name of the admin i.e. administrator (as defined in the ![config](./core/config.py) file), 
    they will be redirected to admin page.
    2. In the admin page, user will be able to see the list of all pets and their owner.


## How to run this application ?

Please, follow these steps to successfully run this application on your local machine:

1. Make sure you have the following dpendencies in your development environment:

    1.  Python 3.9 (You can be flexible with the version but, application is tested with this version only).
    2.  Any IDE or text editor of your choice.
    3.  Access to Command-Line or Terminal.

2. Clone this repository in your working directory.
3. In your terminal or command line, make sure you are pointing to working directory.
4. Now, go to root direcotry of this application i.e. 'python-agent'.
5. Create python virtual environment (Recommended). Make sure you activate the virtual environment.
6. Install the dependencies by typing following command:

      `pip install -r requirements.txt`

7. Initialize the database. This application uses sqlite database. Type following command:
 
    `flask --app core init-db`
 
    This command will create database files inside 'tmp' directory and database tables as defined in the models. 

8. Now the flask application setup is complete, to run it application using development server, type:

      `flask --app core run `
## Check agents status
### On pod
checking that your agent sent observer data to collector and Collector sent data to APM server.
```
kubectl logs <collector pod name>
```
**Result**
It will show detail about observer data that you sent to APM server.
<div style="text-align:center"><img src="/OTEL Agent/Python-agent/images/pod_status.png" /></div>

### On kibana
GO to kibana -> `intergration`-> `APM`. Click Opentelemerty and select your fleet polices that you created on [ELK Stack](https://gitlabii.thaibevapp.com/devops/ea/eck/-/tree/master/ELK%20Stack) manual.

<div style="text-align:center"><img src="/OTEL Agent/Python-agent/images/agent_check.png"/></div>

After that click `check agent status` if it show green, your agent is work!

<div style="text-align:center"><img src="/OTEL Agent/Python-agent/images/agent_check02.png"/></div>

## Visualization on kibana
### Trace
1. Search and Click `APM` in search bar
<div style="text-align:center"><img src="/OTEL Agent/Python-agent/images/apm.png"/></div>

2. Go to `Services` and find service `python_petshop`
<div style="text-align:center"><img src="/OTEL Agent/Python-agent/images/services.png"/></div>

3. Scroll down, you will all transcations of service. Click on transcations that you want to see.
<div style="text-align:center"><img src="/OTEL Agent/Python-agent/images/trans01.png"/></div>

4. In this picture choose /buy
<div style="text-align:center"><img src="/OTEL Agent/Python-agent/images/trans.png"/></div>

### Metric
> # ATTENTION!
> Agent can send 3 type of metrics (Count, Gauge and Histogram). In this code example send only Gauge but don't worry i will show how to find all of metrics type.
<div style="text-align:center"><img src="/OTEL Agent/Python-agent/images/Metric.png"/></div>

Click on `Metrics Explorer`. At graph per search for `service.name`. If you want to see `Gauge` select **Average** ,**Max** ,**Min** ,**Sum** (red box). 
For `Count` select **Document Count** (blue box).

In This code example send only Gauge metrics name **cpu_usage** at service name **python_petshop_view**.In Green box choose cpu_usage.

<div style="text-align:center"><img src="/OTEL Agent/Python-agent/images/gauge.png"/></div>


    

