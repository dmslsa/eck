from core import create_app

if __name__ == "__main__":
    app = create_app()
    app.run(host="localhost", port=3000)
from opentelemetry.instrumentation.flask import FlaskInstrumentor

FlaskInstrumentor().instrument_app(app)