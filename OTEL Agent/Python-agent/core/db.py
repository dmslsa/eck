"""
Contains singleton class for Database instance.
"""

from flask_sqlalchemy import SQLAlchemy
from opentelemetry.instrumentation.sqlalchemy import SQLAlchemyInstrumentor
from opentelemetry.sdk.resources import Resource,SERVICE_NAME
from opentelemetry.sdk.trace import TracerProvider
from opentelemetry.sdk.trace.export import BatchSpanProcessor
from opentelemetry.exporter.otlp.proto.grpc.trace_exporter import OTLPSpanExporter
from opentelemetry import trace
import os
from dotenv import load_dotenv
load_dotenv()
endpoint = os.getenv('ENDPOINT')

resource = Resource(attributes={SERVICE_NAME: "python.petshop/db"})
tracer_provider = TracerProvider(resource=resource)
    #export to collector
tracer_provider.add_span_processor(BatchSpanProcessor(OTLPSpanExporter(endpoint=endpoint)))
    # export to console
    # tracer_provider.add_span_processor(BatchSpanProcessor(ConsoleSpanExporter()))
trace.set_tracer_provider(tracer_provider)
tracer = trace.get_tracer_provider().get_tracer("python-petshop")

class Database:
    """
    Singleton class to interact with the database
    """
    __instance = None
   

    @staticmethod
    def get_instance() -> SQLAlchemy:
        with tracer.start_as_current_span("DB_get_instance"):
            """
        Returns SQLAlchemy database instance.
        Creates if not already initialized
        """
            if not Database.__instance:
                Database()
            return Database.__instance

    def __init__(self):
        if Database.__instance:
            raise Exception("Only one instance of Database is allowed")
        else:
            Database.__instance = SQLAlchemy()

    @classmethod
    def init_db(cls):
        """
        Initializes database by creating all the tables
        defined in the models
        """
        cls.get_instance().create_all()

    @classmethod
    def insert(cls, model_instance):
        with tracer.start_as_current_span("DB_insert"):
            """
        Inserts record in the database table as
        represented by model instance
        :param model_instance: Instance of any model that needs to be inserted
        """
            cls.get_instance().session.add(model_instance)
            cls.get_instance().session.commit()

    @classmethod
    def insert_all(cls, model_instances):
        with tracer.start_as_current_span("DB_insert_all"):
            """
        Inserts multiple records in the database as
        represented by list of model instances
        :param model_instances: List of model instances
        """
            cls.get_instance().session.add_all(model_instances)
            cls.get_instance().session.commit()

    @classmethod
    def close_db(cls, e=None):
        """
        Terminates database connection
        """
        cls.get_instance().session.close()

SQLAlchemyInstrumentor().instrument()