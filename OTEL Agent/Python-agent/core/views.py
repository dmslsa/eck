"""
Contains definition and business logic for each route handled by the package
"""
from typing import Iterable
from flask import Blueprint, render_template, request, redirect, url_for
from .config import ADMIN_NAME
from .controller import OwnerController, PetController
from opentelemetry import trace, metrics
from opentelemetry.metrics import CallbackOptions,Observation
from opentelemetry.instrumentation.flask import FlaskInstrumentor
from opentelemetry.sdk.metrics import MeterProvider
from opentelemetry.sdk.metrics.export import PeriodicExportingMetricReader, ConsoleMetricExporter
from opentelemetry.sdk.resources import Resource,SERVICE_NAME
from opentelemetry.sdk.trace import TracerProvider
from opentelemetry.sdk.trace.export import BatchSpanProcessor,ConsoleSpanExporter
from opentelemetry.exporter.otlp.proto.grpc.trace_exporter import OTLPSpanExporter
from opentelemetry.exporter.otlp.proto.grpc.metric_exporter import OTLPMetricExporter
import psutil
import os
from dotenv import load_dotenv
# Blueprints in flask are used to de-couple application into smaller
# re-usable components. Blueprints are registered with the flask
# application
bp = Blueprint('view', __name__, url_prefix='/')


load_dotenv()
endpoint = os.getenv('ENDPOINT')
resource = Resource(attributes={SERVICE_NAME: "python.petshop/view"})
tracer_provider = TracerProvider(resource=resource)
    #export to collector
tracer_provider.add_span_processor(BatchSpanProcessor(OTLPSpanExporter(endpoint=endpoint)))
    # export to console
# tracer_provider.add_span_processor(BatchSpanProcessor(ConsoleSpanExporter()))
trace.set_tracer_provider(tracer_provider)
tracer = trace.get_tracer_provider().get_tracer("python.trace")

# Metrics
#export to collector
reader = PeriodicExportingMetricReader(
    OTLPMetricExporter(endpoint=endpoint)
)
# export to console
# reader = PeriodicExportingMetricReader(ConsoleMetricExporter()) 
Metrics_provider = MeterProvider(resource=resource, metric_readers=[reader])
meter = metrics.get_meter(__name__)


def get_cpu_usage(options: CallbackOptions) -> Iterable[Observation]:
    cpu_usage_percent = psutil.cpu_percent(interval=None)
    print("cpu_usage_percent")
    print(cpu_usage_percent )
    observation = Observation(value=cpu_usage_percent)
    print(observation)
    return [observation]

# create a metrics that collect cpu usage
cpu_usage_metric = meter.create_observable_gauge(
    name="cpu_usage",
    description="CPU Usage",
    unit="percent",
    callbacks=[get_cpu_usage]
)
metrics.set_meter_provider(Metrics_provider)
# @tracer.start_as_current_span("do_work")
@bp.route("/", methods=["GET", "POST"])
def welcome():
    with tracer.start_as_current_span("web-request"):
        """
        GET: Returns welcome.html template
        POST: Inserts owner with the given fullname in the database
        if it does not exist. Then, redirects the owner to the home page
    (home.html). If given fullname belongs to the admin then redirects to
    admin page (admin.html)
    """
        if request.method == "POST":
            
            fullname = request.form.get('fullname')

            if fullname == ADMIN_NAME:
                return redirect(url_for('view.admin'))

            fullname_stripped = fullname.strip()
            owner_o = OwnerController.get_owner(fullname_stripped)
            if not owner_o:
                owner_o = OwnerController.insert_owner(fullname_stripped)
            return redirect(url_for('view.home', username=owner_o.username))

        return render_template("welcome.html")


@bp.route("/admin", methods=["GET"])
def admin():
    with tracer.start_as_current_span("admin"):
    # instrument()
        """
    Returns admin page, with the list of all the pets
    with their owners.
    """

        pets = PetController.get_all()
        return render_template("admin.html", pets=[pet.serialize for pet in pets])


@bp.route("/home/<username>", methods=["GET"])
def home(username):
    with tracer.start_as_current_span("home/user"):
    # instrument()
   
        """
        Returns home page, with list of all the pets belonging to the
        owner identified by given username.
        """
        pets = OwnerController.get_pets(username)
        return render_template("home.html", username=username, pets=[pet.serialize for pet in pets])


@bp.route("/buy", methods=["POST"])
def buy_pet():
    with tracer.start_as_current_span("/buy"):
        """
    Inserts pets recorded with the owner who bought the pet in the database
    """

        pet_type = request.form.get('pet')
        owner_username = request.form.get('username')
        owner_o = OwnerController.get_owner(identifier=owner_username)
        PetController.insert_pet(pet_type, owner_o.fullname)

        return redirect(url_for('view.home', username=owner_username))

