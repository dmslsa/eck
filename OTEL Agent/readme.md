# OTEL Agent
## Python Agent
:white_check_mark: **Trace**

**Metrcis**
:white_check_mark: Gauges
:black_square_button: Histogram
:black_square_button: Count 

Document:
1. [python-OTEL SDK](https://opentelemetry-python.readthedocs.io/en/latest/sdk/index.html)
2. [python-OTEL DOC ](https://opentelemetry-python.readthedocs.io/en/latest/api/index.html)
3. [Python Example](https://github.com/open-telemetry/opentelemetry-python/tree/stable/docs/examples)

## Nodejs Agent
:white_check_mark: **Trace**

**Metrcis**
:white_check_mark: Gauges
:white_check_mark: Histogram
:white_check_mark: Count 

Document:
1. [Nodejs Example](https://github.com/open-telemetry/opentelemetry-js/tree/main/examples/otlp-exporter-node)

## GO Agent
