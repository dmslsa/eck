# SET UP OTEL COLLECTOR
>## REMINDER
>Before deploy Collector, you must deploy ECK first. By follows the [ECK manual](https://gitlabii.thaibevapp.com/devops/ea/eck/-/tree/master/ELK%20Stack). 
> **Requirement:**
> - APM server
> - ECK Stack
> - Fleet server that connect to APM

<div style="text-align:center"><img src="/OTEL Collector/image/change_v.png"/></div>
don't forget to change `endpoint` to your APM IP and change `Authorization` to your APM token

## 1. Get your APM Token
check your APM name
```
kubectl get service --selector='common.k8s.elastic.co/type=apm-server'
```
<div style="text-align:center"><img src="/OTEL Collector/image/apmname.png"/></div>

get APM Token
```
kubectl get secret/<APM-server-name>-apm-token -o go-template='{{index .data "secret-token" | base64decode}}'
```
<div style="text-align:center"><img src="/OTEL Collector/image/token.png"/></div>

After Change endpoint and token.
```
Kubectl apply -f Opentelemerty-Collector-config.yaml
```
## 2.Check Collector Status
```
kubectl logs <collector pod name>
```
if it shows `Everything is ready. Begin running and processing data`, Collector's ready to use.
<div style="text-align:center"><img src="/OTEL Collector/image/alright.jpg"/></div>


